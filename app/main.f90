!> \file    main.f90
!> \brief   This file will be the executable.
!> \details You can also give more information about this file.
!> \version 0.1
!> \authors Sebastian Mueller
!> \date    May 2021

!> \brief   Example application
!> \details This program performs something very simple. But you should describe it.
!> \version 0.1
!> \authors Sebastian Mueller
!> \date    May 2021
program main

  use mo_kind,              only: dp
  use mo_lib_functions,     only: set_global_vars, set_container_vars, print_globals
  use mo_lib_info,          only: version, version_date

  implicit none

  real(dp), dimension(3) :: dat

  dat = [1., 2., 3.]

  call set_global_vars(1, 2.0_dp, dat)
  call set_container_vars(2, 3.0_dp, 2.0 * dat)

  call print_globals

  call set_global_vars(10, 20.0_dp, 10 * dat)
  call set_container_vars(20, 30.0_dp, 20 * dat)

  call print_globals

end program main
