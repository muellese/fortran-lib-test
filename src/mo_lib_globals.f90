!> \file    mo_lib_globals.f90
!> \brief   Example module providing global variables
!> \details This file contains multiple global variables.
!> \version 0.1
!> \authors Sebastian Mueller
!> \date    Jul 2021

!> \brief   Example module providing global variables
!> \details This module contains multiple global variables.
!> \version 0.1
!> \authors Sebastian Mueller
!> \date    Jul 2021
module mo_lib_globals

  use mo_kind,   only: i4, dp

  implicit none

  type global_type
    integer(i4) :: type_int
    real(dp) :: type_real
    real(dp), dimension(:), allocatable :: type_array
  end type global_type

  type(global_type), public :: global_container
  integer(i4), public :: global_int
  real(dp), public :: global_real
  real(dp), dimension(:), allocatable, public :: global_array

end module mo_lib_globals
