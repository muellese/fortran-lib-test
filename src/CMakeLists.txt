list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../cmake/cmake-modules)

set(LIB_NAME "${PROJECT_NAME}-lib")

# add sources
file(GLOB sources ./mo_*.f90 ./mo_*.F90)
add_library(${LIB_NAME} ${sources})
target_include_directories(${LIB_NAME} PUBLIC ${CMAKE_CURRENT_BINARY_DIR})

# download forces v0.1
include(FetchContent)
set (FORCES_NAME forces)
FetchContent_Declare(
  ${FORCES_NAME}
  GIT_REPOSITORY https://git.ufz.de/chs/forces.git
  GIT_TAG        v0.1.0
  SOURCE_SUBDIR  src
)
set (${FORCES_NAME}_BUILD_TESTING OFF)
FetchContent_MakeAvailable(${FORCES_NAME})
target_link_libraries(${LIB_NAME} PUBLIC ${FORCES_NAME})

# add pre-processor flag (-cpp or -fpp)
include(fortranpreprocessor)
get_preproc_flag(XPP_FLAG)
target_compile_options(${LIB_NAME} PUBLIC ${XPP_FLAG})

# add all compile options (MPI, OpenMP, Lapack, Coverage)
include(compileoptions)
if (CMAKE_WITH_MPI)
  target_compile_definitions(${LIB_NAME} PRIVATE MPI)
  target_link_libraries(${LIB_NAME} PUBLIC MPI::MPI_Fortran)
endif()
if (CMAKE_WITH_OpenMP)
  target_link_libraries(${LIB_NAME} PUBLIC OpenMP::OpenMP_Fortran)
endif()

# set compiling flags for debug and relese version
if(CMAKE_Fortran_COMPILER_ID MATCHES "GNU")
  target_compile_definitions(${LIB_NAME} PUBLIC "GFORTRAN")
  target_compile_options(${LIB_NAME} PUBLIC
    -ffree-form -ffixed-line-length-132
    $<$<CONFIG:DEBUG>:-pedantic-errors -Wall -W -O -g -Wno-maybe-uninitialized>
    $<$<CONFIG:RELEASE>:-O3>
  )
endif()
if(CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
  target_compile_definitions(${LIB_NAME} PUBLIC "INTEL")
  target_compile_options(${LIB_NAME} PUBLIC
    -nofixed "SHELL:-assume byterecl" "SHELL:-fp-model source" -m64 "SHELL:-assume realloc-lhs"
    $<$<CONFIG:DEBUG>:-g "SHELL:-warn all" "SHELL:-check all" -debug -traceback -fp-stack-check -O0>
    $<$<CONFIG:RELEASE>:-O3 -qoverride-limits>
  )
endif()
if(CMAKE_Fortran_COMPILER_ID MATCHES "NAG")
  target_compile_definitions(${LIB_NAME} PUBLIC "NAG")
  target_compile_options(${LIB_NAME} PUBLIC
    -fpp -colour -unsharedf95 -ideclient
    $<$<CONFIG:DEBUG>:-g -nan -O0 -C -C=alias -C=dangling -strict95 -ieee=full>
    $<$<CONFIG:RELEASE>:-O4 -ieee=full>
  )
endif()
