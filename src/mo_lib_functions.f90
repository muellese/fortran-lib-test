!> \file    mo_lib_functions.f90
!> \brief   Example module providing functions to manipulate global vars.
!> \version 0.1
!> \authors Sebastian Mueller
!> \date    Jul 2021

!> \brief   Example module providing functions to manipulate global vars.
!> \version 0.1
!> \authors Sebastian Mueller
!> \date    Jul 2021
module mo_lib_functions

  use mo_kind,   only: i4, dp

  implicit none
  private
  public :: set_global_vars
  public :: get_global_vars
  public :: set_container_vars
  public :: get_container_vars
  public :: print_globals

contains

  !> \brief   set global vars
  subroutine set_global_vars(int_in, real_in, array_in)

    use mo_lib_globals, only: global_int, global_real, global_array

    implicit none

    integer(i4), intent(in) :: int_in !< integer input
    real(dp), intent(in) :: real_in !< real input
    real(dp), dimension(:), intent(in) :: array_in !< array input

    global_int = int_in
    global_real = real_in
    if (allocated(global_array)) deallocate(global_array)
    allocate(global_array(size(array_in)))
    global_array = array_in

  end subroutine set_global_vars

  !> \brief   get global vars
  subroutine get_global_vars(int_out, real_out, array_out)

    use mo_lib_globals, only: global_int, global_real, global_array

    implicit none

    integer(i4), intent(out) :: int_out !< integer input
    real(dp), intent(out) :: real_out !< real input
    real(dp), dimension(:), intent(out) :: array_out !< array input

    int_out = global_int
    real_out = global_real
    array_out = global_array

  end subroutine get_global_vars

  !> \brief   set container vars
  subroutine set_container_vars(int_in, real_in, array_in)

    use mo_lib_globals, only: global_container

    implicit none

    integer(i4), intent(in) :: int_in !< integer input
    real(dp), intent(in) :: real_in !< real input
    real(dp), dimension(:), intent(in) :: array_in !< array input

    global_container%type_int = int_in
    global_container%type_real = real_in
    if (allocated(global_container%type_array)) deallocate(global_container%type_array)
    allocate(global_container%type_array(size(array_in)))
    global_container%type_array = array_in

  end subroutine set_container_vars

  !> \brief   get container vars
  subroutine get_container_vars(int_out, real_out, array_out)

    use mo_lib_globals, only: global_container

    implicit none

    integer(i4), intent(out) :: int_out !< integer input
    real(dp), intent(out) :: real_out !< real input
    real(dp), dimension(:), intent(out) :: array_out !< array input

    int_out = global_container%type_int
    real_out = global_container%type_real
    array_out = global_container%type_array

  end subroutine get_container_vars

  !> \brief   print globals
  subroutine print_globals()

    use mo_lib_globals, only: global_container, global_int, global_real, global_array

    implicit none

    print*, "global int:", global_int
    print*, "global real:", global_real
    print*, "global array:", global_array

    print*, " "
    print*, "container int:", global_container%type_int
    print*, "container real:", global_container%type_real
    print*, "container array:", global_container%type_array

  end subroutine print_globals

end module mo_lib_functions
