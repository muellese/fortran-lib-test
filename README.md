# Fortran Lib Test

This project is a fortran library that provides global variables and interfaces to manipulated them.
In addition, this library is also linking against [FORCES](https://git.ufz.de/chs/forces/).

The purpose of this library is to be later used by a python wrapper.

(C) Sebastian Mueller